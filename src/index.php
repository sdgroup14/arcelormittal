<!DOCTYPE html>
<html>

<head>
	<meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>
	<meta name="format-detection" content="telephone=no">
	<meta name="viewport" content="width=device-width, initial-scale=1.0">

	<!-- build:remove -->
	<link href="css/style.css" rel="stylesheet">
	<!-- /build -->
	<!-- build:include ../templates/css-link.html -->
	<!-- /build -->
	<title>ArcelorMittal</title>
</head>
<body>

	<?php include 'views/pages/main.html'; ?>

	<div class="loader-wrapper">
		<div class="loader">
			<div class="sk-folding-cube">
				<div class="sk-cube1 sk-cube"></div>
				<div class="sk-cube2 sk-cube"></div>
				<div class="sk-cube4 sk-cube"></div>
				<div class="sk-cube3 sk-cube"></div>
			</div>
		</div>
	</div>

<script src="js/libs/jquery.min.js"></script>
<!-- build:remove -->
<script src="js/events.js"></script>
<!-- /build -->
<!-- build:include ../templates/js-script.html -->
<!-- /build -->


</body>
</html>