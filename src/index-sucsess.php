<!DOCTYPE html>
<html>

<head>
	<meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>
	<meta name="format-detection" content="telephone=no">
	<meta name="viewport" content="width=device-width, initial-scale=1.0">

	<!-- build:remove -->
	<link href="css/style.css" rel="stylesheet">
	<!-- /build -->
	<!-- build:include ../templates/css-link.html -->
	<!-- /build -->
	<title>ArcelorMittal</title>
</head>
<body>

<?php include 'views/pages/main-sucsess.html'; ?>

<div class="loader-wrapper">
	<div class="loader">
		<div class="sk-folding-cube">
			<div class="sk-cube1 sk-cube"></div>
			<div class="sk-cube2 sk-cube"></div>
			<div class="sk-cube4 sk-cube"></div>
			<div class="sk-cube3 sk-cube"></div>
		</div>
	</div>
</div>

<!--<div class="loader-wrapper">-->
<!--<div class="loader">-->
<!--<div class="sk-folding-cube">-->
<!--<div class="sk-cube1 sk-cube"></div>-->
<!--<div class="sk-cube2 sk-cube"></div>-->
<!--<div class="sk-cube4 sk-cube"></div>-->
<!--<div class="sk-cube3 sk-cube"></div>-->
<!--</div>-->
<!--</div>-->
<!--</div>-->

<script src="js/libs/jquery.min.js"></script>

<!-- build:remove -->
<!--<script src="js/events.js"></script>-->
<!-- /build -->
<!-- build:include ../templates/js-script.html -->
<!-- /build -->
<script>
	$(document).ready(function () {

		var $preloader = $('.loader-wrapper');
		$spinner = $preloader.find('.loader');
		$spinner.delay(2500).fadeOut();
		$preloader.delay(3000).fadeOut('slow');
		setTimeout(function () {
			window.onscroll = function (e) {  };
		}, 3000)

		var _check_flag;

		$('.checkbox').change(function() {
			$('.answ_err-container').hide();
			if (this.checked) {
				_check_flag = 1;
			} else {
				_check_flag = 0;
			}
		});

		$('.agree-btn').on('click', function (e) {
			if(!_check_flag){
				e.preventDefault();
				$('.answ_err-container').show();
			} else {
				return
			}
		});

	});
</script>

</body>
</html>