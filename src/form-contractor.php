<!DOCTYPE html>
<html>

<head>
	<meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>
	<meta name="format-detection" content="telephone=no">
	<meta name="viewport" content="width=device-width, initial-scale=1.0">

	<!-- build:remove -->
	<link href="css/style.css" rel="stylesheet">
	<!-- /build -->
	<!-- build:include ../templates/css-link.html -->
	<!-- /build -->
	<title>ArcelorMittal</title>
</head>
<body>

<?php include 'views/pages/form/head.html'; ?><?php include 'views/pages/form/content.php'; ?><?php include 'views/pages/form/footer.html'; ?>


<script src="js/libs/jquery.min.js"></script>
<!-- build:remove -->
<!--<script src="js/events.js"></script>-->
<!-- /build -->
<!-- build:include ../templates/js-script.html -->
<!-- /build -->
<script>
	var file_api = (window.File && window.FileReader && window.FileList && window.Blob) ? true : false;
	var wrapper = $(".file_upload"),
		inp = wrapper.find(".attach_inp"),
		btn = wrapper.find(".attach_inp-txt"),
		lbl = wrapper.find("div");

	$('body').on('change', '.attach_inp', function () {
		var file_name;
		if (file_api && $(this)[0].files[0]) {
			file_name = $(this)[0].files[0].name;
			console.log(file_name);
		}
		else
			file_name = $(this).val().replace("C:\\fakepath\\", '');

		if (!file_name.length)
			return;
		else {

			$(this).closest('.attach-f-wrap').find('.attach_inp-txt').text(file_name);
			$(this).closest('.attach-f-wrap').find('svg').css('fill', '#ff3700');

		}
	});

	$('.cat-head').on('click', function () {
		$(this).closest('.form-catigories').toggleClass('toggle-cat');
		console.log('123');
	})

	// $('.form-catigories').addClass('toggle-cat');


	var table_row_template = '<div class="subcat-t-row"> <div class="subcat-t-cell"><textarea rows="1"></textarea></div> <div class="subcat-t-cell"><textarea rows="1"></textarea></div> <div class="subcat-t-cell"><textarea rows="1"></textarea></div> <div class="subcat-t-cell"><textarea rows="1"></textarea></div> <div class="subcat-t-cell"><textarea rows="1"></textarea></div> <div class="subcat-t-cell"><textarea rows="1"></textarea></div> <div class="subcat-t-cell"><textarea rows="1"></textarea></div> </div>';
	var textarea = '<div class="subcat-row"> <textarea rows="1"></textarea> </div>';

	var table_row_attach_template = function (n) {
		return '<div class="subcat-t-row"><div class="subcat-t-cell"><textarea rows="1"></textarea></div> <div class="subcat-t-cell"><textarea rows="1"></textarea></div> <div class="subcat-t-cell"><textarea rows="1"></textarea></div> <div class="subcat-t-cell"><textarea rows="1"></textarea></div> <div class="subcat-t-cell"><textarea rows="1"></textarea></div> <div class="subcat-t-cell"><textarea rows="1"></textarea></div> <div class="subcat-t-cell"> <div class="attach-f"> <div class="attach-f-wrap"> <input type="file" id="attach_inp-' + n + '" class="attach_inp"/> <label for="attach_inp-' + n + '" class="attach-label"> <svg class="icon-svg-attach"> <use xlink:href="img/sprite.svg#attach" xmlns:xlink="http://www.w3.org/1999/xlink"></use> </svg> </label> <div class="attach_inp-txt"></div> </div> </div> </div> </div>';
	}

	$('.add-t-row').on('click', function () {
		$(this).closest('.t-container').find('.subcat-table').append(table_row_template);
	});

	$('.add-textarea').on('click', function () {
		$(this).closest('.cat-row_q_box').find('.dynamic-textarea-container').append(textarea);
	});

	$('.add-t-row-123').on('click', function (e) {

		var last = $(this).closest('.t-container').find('.subcat-t-row').last().clone();

		var row = $(last).find('textarea').last().attr('id').match(/([a-z]+)\-([a-z]+)+\-(\d+)\-(\d+)-(\d+)/)[5],
			cell = $(last).find('textarea').last().attr('id').match(/([a-z]+)\-([a-z]+)+\-(\d+)\-(\d+)-(\d+)/)[4];
		row = '' + (+row + 1) + '';
		$(last).find('textarea').each(function () {
			var text_id = $(this).attr('id');
			var text_name = $(this).attr('name');
			var text_id = text_id.match(/([a-z]+)\-([a-z]+)+\-(\d+)\-(\d+)-(\d+)/);
			var text_name = text_name.match(/([a-zA-Z]+)\[([a-zA-Z]+)\]\[(\d+)+\]\[(\d+)\]\[(\d+)\]/);
			text_name = text_name.slice(1, 6);
			text_id = text_id.slice(1, 6);
			text_id[4] = row;
			text_name[4] = row;
			text_id = text_id.join('-');

			$.each(text_name, function (key, value) {
				if (key) {
					text_name[key] = '[' + value + ']';
				}
			});

			text_name = text_name.join('');
			$(this).attr('id', text_id);
			$(this).attr('name', text_name);
			$(this).val('');
		});

		$(last).find('.attach-f-wrap').each(function () {

			var text_id = $(this).find('.attach_inp').attr('id');
			var text_name = $(this).find('.attach_inp').attr('name');
			var label_name = $(this).find('label').attr('for');
			var text_id = text_id.match(/([a-z]+)\-([a-z_]+)+\-(\d+)\-(\d+)/);
			var label_name = label_name.match(/([a-z]+)\-([a-z_]+)+\-(\d+)\-(\d+)/);
			var text_name = text_name.match(/([a-zA-Z]+)\[([a-zA-Z_]+)\]\[(\d+)+\]\[(\d+)\]/);
			text_name = text_name.slice(1, 5);
			console.log(text_name);
			text_id = text_id.slice(1, 5);
			console.log(text_id);
			label_name = label_name.slice(1, 5);
			text_id[3] = row;
			label_name[3] = row;
			text_name[3] = row;
			text_id = text_id.join('-');
			label_name = label_name.join('-');

			$.each(text_name, function (key, value) {
				if (key) {
					text_name[key] = '[' + value + ']';
					console.log(key + ": " + value);
				}
			});

			text_name = text_name.join('');
			$(this).find('.attach_inp').attr('id', text_id);
			$(this).find('label').attr('for', label_name);
			$(this).find('.attach_inp').attr('name', text_name);
			$(this).find('.attach_inp').val('');
			$(this).closest('.subcat-t-cell').find('.attach_inp-txt').text('');
			$(this).closest('.subcat-t-cell').find('.answ_err-txt').text('');
			$(this).closest('.subcat-t-cell').find('.answ_err-container-attach').css('opacity', '0');
			console.log($(this).closest('.subcat-t-cell').find('.answ_err-txt'));
		});

		$(this).closest('.t-container').find('.subcat-table').append(last);

		$(last).find('textarea').val('');

		last = null;
	});

	$(document).ready(function () {
		$('.form-head').css('padding-top', $('.fixedContent').height());
	});
</script>

</body>

<!--<div class="subcat-t-row">-->
<!--<div class="subcat-t-cell"><textarea name="able[1][3][1]" rows="1"></textarea></div>-->
<!--<div class="subcat-t-cell"><textarea name="able[1][3][1]"rows="1"></textarea></div>-->
<!--<div class="subcat-t-cell"><textarea rows="1"></textarea></div>-->
<!--<div class="subcat-t-cell"><textarea rows="1"></textarea></div>-->
<!--<div class="subcat-t-cell"><textarea rows="1"></textarea></div>-->
<!--<div class="subcat-t-cell"><textarea rows="1"></textarea></div>-->
<!--<div class="subcat-t-cell">-->
<!--<div class="attach-f">-->
<!--<div class="attach-f-wrap"><input type="file" id="attach_inp-' + n + '" class="attach_inp"/> <label for="attach_inp-' + n + '" class="attach-label">-->
<!--<svg class="icon-svg-attach">-->
<!--<use xlink:href="img/sprite.svg#attach" xmlns:xlink="http://www.w3.org/1999/xlink"></use>-->
<!--</svg>-->
<!--</label>-->
<!--<div class="attach_inp-txt"></div>-->
<!--</div>-->
<!--</div>-->
<!--</div>-->
<!--</div>-->


</html>