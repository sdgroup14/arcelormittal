window.onscroll = function (e) { window.scrollTo(0, 0); };

$(document).ready(function () {

	var $preloader = $('.loader-wrapper');
	$spinner = $preloader.find('.loader');
	$spinner.delay(2500).fadeOut();
	$preloader.delay(3000).fadeOut('slow');
	setTimeout(function () {
		window.onscroll = function (e) {  };
	}, 3000)

	var top_form_data,
	bottom_form_data,
	filter,
	email;

	$(".top-select").change(function () {
		if ($(this).val()) {
			$(this).closest('.top-select-common').find('.err').removeClass('err__active');
		} else return
	});

	$('.top-form-btn').on('click', function (e) {
		e.preventDefault();
		top_form_data = $('#top-form').serializeArray();

		if (top_form_data.length !== 2) {
			console.log('Top form err');
			$('.top-select').each(function (i, t) {
				if (!$(this).val()) {
					console.log('show err');
					$(this).closest('.top-select-common').find('.err').addClass('err__active');
				} else return
			});
		} else {
			console.log('Top form send');
		}
	});

	$(".b-form-inp").on('input', function () {
		if ($(this).val()) {
			$('.b-form').find('.err').removeClass('err__active');
		} else return
	});

	$('.b-form-btn').on('click', function (e) {
		e.preventDefault();
		email = $('.b-form-inp').val();
		filter = /^([\w-\.]+)@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.)|(([\w-]+\.)+))([a-zA-Z]{2,4}|[0-9]{1,3})(\]?)$/;
		bottom_form_data = $('#b-mail').serializeArray();
		$(this).closest('.b-form').find('.err').removeClass('err__active');
		if(!$('.b-form-inp').val()) {
			console.log('empty');
			$('.err__empty').addClass('err__active');
		}
		else if($('.b-form-inp').val().length > 100) {
			console.log('value more then 100 char');
			$('.err__not_length').addClass('err__active');
		}
		else if(!filter.test(email)) {
			console.log('not valid');
			$('.err__not_valid').addClass('err__active');
		} else {
			console.log('email send');
			$('.mailing-first-step').hide();
			$('.mailing-second-step').show();
			$("html, body").animate({ scrollTop: 9999 }, 4000);

			$('.email-data').text($('.b-form-inp').val());
			resendEmail();
		}
	});

	$('.resend-letter').on('click', function () {
		resendEmail();
		$('.timer').text('59');
	});

	function resendEmail() {
		var counter = 59;
		$('.resend-letter').removeClass('active');
		setInterval(function() {
			counter--;
			if (counter >= 0) {
				$('.timer').text(counter);
			}
			if (counter === 0) {
				clearInterval(counter);
				$('.resend-letter').addClass('active');

			}
		}, 1000);
	}


});


