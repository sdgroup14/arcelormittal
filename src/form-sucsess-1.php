<!DOCTYPE html>
<html>

<head>
	<meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>
	<meta name="format-detection" content="telephone=no">
	<meta name="viewport" content="width=device-width, initial-scale=1.0">

	<!-- build:remove -->
	<link href="css/style.css" rel="stylesheet">
	<!-- /build -->
	<!-- build:include ../templates/css-link.html -->
	<!-- /build -->
	<title>ArcelorMittal</title>
</head>
<body>

<?php include 'views/pages/form-sucsess-1.html'; ?>


<script src="js/libs/jquery.min.js"></script>
<!-- build:remove -->
<!--<script src="js/events.js"></script>-->
<!-- /build -->
<!-- build:include ../templates/js-script.html -->
<!-- /build -->
<script>

</script>

</body>
</html>